import React from 'react'


const GlassesItem = ({ glasses, handleDetail }) => {
    return (
        <div className='col-2 border-1 border border-dark'>
            <a type='button' onClick={() => handleDetail(glasses)}><img className='img-fluid' src={glasses.url} alt="" /></a>
        </div>
    )
}

export default GlassesItem