import React from 'react'
import GlassesItem from './GlassesItem'

const GlassesList = ({ data, handleDetail }) => {
    return (
        <div className='row gap-4 px-5'>
            {
                data.map((glasses) => <GlassesItem key={glasses.id} glasses={glasses} handleDetail={handleDetail} />)
            }
        </div>
    )
}

export default GlassesList