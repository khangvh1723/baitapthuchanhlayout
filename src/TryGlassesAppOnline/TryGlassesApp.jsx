import React, { useState } from 'react'
import Test from './Test'
import data from './data.json'
import './scss/styles.scss'
import GlassesList from './GlassesList'

const TryGlassesApp = () => {
    const [glassesDetail, setGlassesDetail] = useState(data[0])
    const handleDetail = (glasses) => {
        setGlassesDetail(glasses)
    }
    return (
        <div className='TryGlassesApp'>
            {/* header start */}
            <header className='Header'>
                <h1 className='text_heading text-uppercase text-center'>try glasses app online</h1>
            </header>
            {/* header end */}
            <div className='container'>
                <Test glassesDetail={glassesDetail} />
                <GlassesList data={data} handleDetail={handleDetail} />
            </div>
        </div>
    )
}

export default TryGlassesApp