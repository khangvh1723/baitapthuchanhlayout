import React from 'react'

const Test = ({ glassesDetail }) => {
    return (
        <div className='d-flex justify-content-around py-5'>
            <div className='TestItem'>
                <img className='img-fluid img_test' src="./images/glasses/model.jpg" alt="" />
                <div className='test_visual'>
                    <img className='img-fluid' src={glassesDetail.url} alt={glassesDetail.name} />
                </div>
                <div className='overlay'>
                    <h3 className='text_title'>{glassesDetail.name}</h3>
                    <p className='text_desc'>{glassesDetail.desc}</p>
                </div>
            </div>
            <div className='TestItem'>
                <img className='img-fluid img_test' src="./images/glasses/model.jpg" alt="" />
            </div>
        </div>
    )
}

export default Test