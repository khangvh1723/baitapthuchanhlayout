import React from 'react'
import Item from './Item'

const PageContent = () => {
    return (
        <section className='pt-4'>
            <div className="container px-lg-5">
                <div className="row gx-lg-5">
                    <Item icon={<i class=" bi bi-collection"></i>} textHeader={'Fresh new layout'} desc={'With Bootstrap 5, we\'ve created a fresh new layout for this template!'}></Item>
                    <Item icon={<i class="bi bi-cloud-download"></i>} textHeader={'Free to download'} desc={'As always, Start Bootstrap has a powerful collectin of free templates.'}></Item>
                    <Item icon={<i class="bi bi-card-heading"></i>} textHeader={'Jumbotron hero header'} desc={'The heroic part of this template is the jumbotron hero header!'}></Item>
                    <Item icon={<i class="bi bi-bootstrap"></i>} textHeader={'Feature boxes'} desc={'We\'ve created some custom feature boxes using Bootstrap icons!'}></Item>
                    <Item icon={<i class="bi bi-code"></i>} textHeader={'Simple clean code'} desc={'We keep our dependencies up to date and squash bugs as they come!'}></Item>
                    <Item icon={<i class="bi bi-patch-check"></i>} textHeader={'A name you trust'} desc={'Start Bootstrap has been the leader in free Bootstrap templates since 2013!'}></Item>
                </div>
            </div >
        </section >
    )
}

export default PageContent