import React from 'react'
import Banner from './Banner'
import PageContent from './PageContent'

const Body = () => {
    return (
        <div>
            <Banner></Banner>
            <PageContent></PageContent>
        </div>
    )
}

export default Body