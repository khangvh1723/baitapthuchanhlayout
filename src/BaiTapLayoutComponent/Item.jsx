import React from 'react'
import './css/styles.css'

const Item = ({ icon, textHeader, desc }) => {
    return (
        <div className='Item col-lg-6 col-xl-4 mb-5'>
            <div className="card bg-light border-0 h-100">
                <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0">
                    <div class="feature bg-primary bg-gradient text-white rounded-3 mb-4 mt-n4">{icon}</div>
                    <div className="fs-4 fw-bold">{textHeader}</div>
                    <div className="mb-0">{desc}</div>
                </div>
            </div>
        </div>
    )
}

export default Item