import React from 'react'
import Header from './Header'
import Body from './Body'
import Footer from './Footer'

const BaiTapThucHanhLayout = () => {
    return (
        <div>
            <Header></Header>
            <Body></Body>
            <Footer></Footer>
        </div>
    )
}

export default BaiTapThucHanhLayout