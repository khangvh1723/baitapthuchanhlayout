import logo from './logo.svg';
import './App.css';
import BaiTapThucHanhLayout from './BaiTapLayoutComponent/BaiTapThucHanhLayout'
import TryGlassesApp from './TryGlassesAppOnline/TryGlassesApp';

function App() {
  return (
    // <BaiTapThucHanhLayout></BaiTapThucHanhLayout>
    <TryGlassesApp />
  );
}

export default App;
